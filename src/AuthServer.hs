import Control.Concurrent (forkIO)
import Control.Monad      (void)
import Network            (PortID(..), listenOn)
import Network.Socket     (accept)
import MoP.Auth.Server    (handleConnection)

main :: IO ()
main =
    loop =<< listenOn (PortNumber 3724)
  where
    loop sock = do
        (conn, _) <- accept sock
        void $ forkIO $ handleConnection conn
        loop sock
