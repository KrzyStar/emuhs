import Control.Concurrent (forkIO)
import Control.Monad      (void)
import Network            (PortID(..), listenOn)
import Network.Socket     (accept)
import MoP.World.Server   (handleConnection)

main :: IO ()
main =
    loop =<< listenOn (PortNumber 8085)
  where
    loop sock = do
        (conn, _) <- accept sock
        void $ forkIO $ handleConnection conn
        loop sock
