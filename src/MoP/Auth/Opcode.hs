{-# OPTIONS_GHC -fno-warn-missing-signatures #-}
{-# LANGUAGE ScopedTypeVariables #-}

module MoP.Auth.Opcode where

import Data.Word

type Opcode     = Word8
type ResultCode = Word8

-- Command opcodes
cmdLogonChallenge            :: Opcode = 0x00
cmdLogonProof                :: Opcode = 0x01
cmdReconnectChallenge        :: Opcode = 0x02
cmdReconnectProof            :: Opcode = 0x03
cmdRealmlist                 :: Opcode = 0x10
cmdXferInitiate              :: Opcode = 0x30
cmdXferData                  :: Opcode = 0x31

-- Result opcodes
errAuthSuccess               :: ResultCode = 0x00
errAuthFailBanned            :: ResultCode = 0x03
errAuthFailUnknownAccount    :: ResultCode = 0x04
errAuthFailIncorrectPassword :: ResultCode = 0x05
errAuthFailAlreadyOnline     :: ResultCode = 0x06
errAuthFailServerBusy        :: ResultCode = 0x08
errAuthFailInvalidVersion    :: ResultCode = 0x09
errAuthFailSuspended         :: ResultCode = 0x0C
