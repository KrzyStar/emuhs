module MoP.Auth.Server where

import Control.Monad (void)
import Control.Monad.State (StateT, execStateT, get, lift, put)
import qualified Data.ByteString as BS
import Network.Socket (Socket, sClose)
import Network.Socket.ByteString (recv)
import Text.Printf
import Text.Show.Pretty

import MoP.Auth.Handler
import MoP.Auth.Protocol

handleRequests :: Socket -> StateT AuthState IO ()
handleRequests conn = do
    state  <- get
    packet <- lift $ recv conn 1024
    lift . putStrLn $ printf "Current state: %s\nReceived packet: %s\nParsed packet: %s\n\n"
                      (ppShow state) (show packet) (ppShow $ parsePacket packet)
    if BS.length packet == 0
        then lift $ sClose conn
        else do
            newState <- lift $ handlePacket conn state packet
            case newState of
                AuthDropped -> lift $ sClose conn
                _           -> put newState >> handleRequests conn

handleConnection :: Socket -> IO ()
handleConnection conn = do
    putStrLn "Client connected\n"
    void $ execStateT (handleRequests conn) AuthInitial
    putStrLn "Client disconnected\n"
