{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}

module MoP.Auth.Packet where

import GHC.Generics

import Control.Monad
import qualified Data.ByteString as BS
import Data.Serialize
import Data.Word

import MoP.Auth.Opcode
import MoP.Common.Packet

-- Logon challenge

data ClientLogonChallengePacket = ClientLogonChallengePacket
        { clcError          :: Word8
        , clcPacketLength   :: Word16
        , clcGameName       :: PNullReverseString
        , clcVersion        :: (Word8, Word8, Word8)
        , clcBuild          :: Word16
        , clcPlatform       :: PNullReverseString
        , clcOS             :: PNullReverseString
        , clcCountry        :: PReverseString 4
        , clcTimezoneBias   :: Word32
        , clcIP             :: (Word8, Word8, Word8, Word8)
        , clcUsernameLength :: Word8
        }
    deriving (Show, Generic)

instance Serialize ClientLogonChallengePacket where
    get = ClientLogonChallengePacket
        <$> getWord8
        <*> getWord16le
        <*> get
        <*> liftM3 (,,) getWord8 getWord8 getWord8
        <*> getWord16le
        <*> get <*> get <*> get
        <*> getWord32le
        <*> liftM4 (,,,) getWord8 getWord8 getWord8 getWord8
        <*> getWord8

data ServerLogonChallengePacket = ServerLogonChallengePacket
        { slcError        :: Word8
        , slcHash_B       :: PString 32
        , slcHash_g       :: PString 1
        , slcHash_N       :: PString 32
        , slcHash_s       :: PString 32
        , slcHash_CRC     :: PString 16
        }
    deriving (Show, Generic)

instance Serialize ServerLogonChallengePacket where
    put pkt = do
        putWord8 cmdLogonChallenge
        putWord8 0
        putWord8 (slcError pkt)
        put (slcHash_B pkt)
        putWord8 0x01
        put (slcHash_g pkt)
        putWord8 0x20
        put (slcHash_N pkt)
        put (slcHash_s pkt)
        put (slcHash_CRC pkt)
        putWord8 0

-- Reconnect challenge

data ClientReconnectChallengePacket = ClientReconnectChallengePacket
        { crcError          :: Word8
        , crcPacketLength   :: Word16
        , crcGameName       :: PNullReverseString
        , crcVersion        :: (Word8, Word8, Word8)
        , crcBuild          :: Word16
        , crcPlatform       :: PNullReverseString
        , crcOS             :: PNullReverseString
        , crcCountry        :: PReverseString 4
        , crcTimezoneBias   :: Word32
        , crcIP             :: (Word8, Word8, Word8, Word8)
        , crcUsernameLength :: Word8
        }
    deriving (Show, Generic)

instance Serialize ClientReconnectChallengePacket where
    get = undefined

data ServerReconnectChallengePacket = ServerReconnectChallengePacket
        { srcError        :: Word8
        , srcProof        :: PString 16
        , srcUnknown      :: PString 16
        , srcSecurityFlag :: Word8
        }
    deriving (Show, Generic)

instance Serialize ServerReconnectChallengePacket where
    put = undefined

-- Logon proof

data ClientLogonProofPacket = ClientLogonProofPacket
        { clpHash_A       :: PString 32
        , clpHash_Mc      :: PString 20
        , clpHash_CRC     :: PString 20
        , clpKeyCount     :: Word8
        , clpSecurityFlag :: Word8
        }
    deriving (Show, Generic)

instance Serialize ClientLogonProofPacket where
    get = ClientLogonProofPacket
        <$> get <*> get <*> get
        <*> getWord8
        <*> getWord8

data ServerLogonProofPacket = ServerLogonProofPacket
        { slpError        :: Word8
        , slpHash_M2      :: PString 20
        , slpAccountFlags :: Word32
        }
    deriving (Show, Generic)

instance Serialize ServerLogonProofPacket where
    put pkt = do
        putWord8 cmdLogonProof
        putWord8 (slpError pkt)
        put (slpHash_M2 pkt)
        putWord32le (slpAccountFlags pkt)
        putWord32le 0
        putWord16le 0

-- Reconnect proof

data ClientReconnectProofPacket = ClientReconnectProofPacket
        { crpHash_R1  :: PString 16
        , crpHash_R2  :: PString 20
        , crpHash_R3  :: PString 20
        , crpKeyCount :: Word8
        }
    deriving (Show, Generic)

instance Serialize ClientReconnectProofPacket where
    get = undefined

data ServerReconnectProofPacket = ServerReconnectProofPacket
        { srpError   :: Word8
        , srpUnknown :: Word16
        }
    deriving (Show, Generic)

instance Serialize ServerReconnectProofPacket where
    put = undefined

-- Realmlist

data ClientRealmlistPacket = ClientRealmlistPacket
    deriving (Show, Generic)

instance Serialize ClientRealmlistPacket where
    get = return ClientRealmlistPacket

data ServerRealmlistEntry = ServerRealmlistEntry
        { sreIcon       :: Word8
        , sreLock       :: Word8
        , sreFlags      :: Word8
        , sreName       :: PNullString
        , sreAddress    :: PNullString -- "x.x.x.x:y"
        , srePopulation :: Word32
        , sreCharCount  :: Word8
        , sreTimezone   :: Word8
        }
    deriving (Show, Generic)

instance Serialize ServerRealmlistEntry where
    put pkt = do
        putWord8 (sreIcon pkt)
        putWord8 (sreLock pkt)
        putWord8 (sreFlags pkt)
        put (sreName pkt)
        put (sreAddress pkt)
        putWord32le (srePopulation pkt)
        putWord8 (sreCharCount pkt)
        putWord8 (sreTimezone pkt)
        putWord8 0x2C
        put gameVersion
        putWord16le gameBuild
      where
        gameVersion = (5, 4, 8) :: (Word8, Word8, Word8)
        gameBuild   = 18414

data ServerRealmlistPacket = ServerRealmlistPacket
        { srEntries      :: [ServerRealmlistEntry]
        }
    deriving (Show, Generic)

instance Serialize ServerRealmlistPacket where
    put pkt = do
        put cmdRealmlist
        putWord16le packetSize
        putWord32le 0
        putWord16le realmCount
        putByteString entries
        putWord8 0x10
        putWord8 0
      where
        entries    = BS.concat $ map (runPut . put) (srEntries pkt)
        packetSize = fromIntegral $ BS.length entries + 8
        realmCount = fromIntegral $ length (srEntries pkt)
