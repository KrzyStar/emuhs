{-# LANGUAGE OverloadedStrings #-}

module MoP.Auth.Database where

import Data.ByteString.Char8 (ByteString)
import Database.PostgreSQL.Simple

import MoP.Common.Database

fetchUser :: ByteString -> IO (Maybe ByteString)
fetchUser username = do
    conn <- dbConnect
    res  <- query conn "select passhash from users where username = ?" (Only username)
    if null res then return Nothing
                else return (Just $ fromOnly $ head res)

setSessionKey :: ByteString -> ByteString -> IO ()
setSessionKey username sessionKey = do
    conn <- dbConnect
    _    <- execute conn "update users set session_key = ? where username = ?" (Binary sessionKey, username)
    return ()
