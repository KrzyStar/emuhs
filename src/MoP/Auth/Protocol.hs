{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}

module MoP.Auth.Protocol where

import GHC.Generics

import Data.ByteString (ByteString)
import Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map
import Data.Maybe
import Data.Serialize (Serialize, Get)
import qualified Data.Serialize as DS

import MoP.Auth.Packet
import MoP.Auth.Opcode
import MoP.Common.Cryptography
import MoP.Common.Hash
import MoP.Common.Packet

type Username = ByteString

data ClientPacket = CLogonChallenge     ClientLogonChallengePacket
                  | CReconnectChallenge ClientReconnectChallengePacket
                  | CLogonProof         ClientLogonProofPacket
                  | CReconnectProof     ClientReconnectProofPacket
                  | CRealmlist          ClientRealmlistPacket
                  deriving (Show, Generic)

parsers :: Map Opcode (Get ClientPacket)
parsers = Map.fromList
    [ (cmdLogonChallenge,     CLogonChallenge     <$> DS.get)
    , (cmdReconnectChallenge, CReconnectChallenge <$> DS.get)
    , (cmdLogonProof,         CLogonProof         <$> DS.get)
    , (cmdReconnectProof,     CReconnectProof     <$> DS.get)
    , (cmdRealmlist,          CRealmlist          <$> DS.get)
    ]

instance Serialize ClientPacket where
    get = do
        cmd <- DS.getWord8
        fromMaybe (fail "unknown opcode!") (Map.lookup cmd parsers)

parsePacket :: ByteString -> Maybe (ClientPacket, ByteString)
parsePacket bs = either (const Nothing) Just $ DS.runGetState (DS.get :: Get ClientPacket) bs 0

buildPacket :: Serialize a => a -> ByteString
buildPacket = DS.runPut . DS.put

buildLogonChallenge :: ChallengeSRP -> ByteString
buildLogonChallenge csrp =
    buildPacket $ ServerLogonChallengePacket
        errAuthSuccess
        (PString . toBytes 32 . _B $ csrp)
        (PString . toBytes 1  $ hg)
        (PString . toBytes 32 $ hN)
        (PString . toBytes 32 . _s $ csrp)
        (PString . toBytes 16 . _CRC $ csrp)

buildLogonProof :: ProofSRP -> ByteString
buildLogonProof psrp =
    buildPacket $ ServerLogonProofPacket
        errAuthSuccess
        (PString . toBytes 20 . _M2 $ psrp)
        0x800000 -- account flags

buildRealmlist :: [ByteString] -> ByteString
buildRealmlist names =
    buildPacket $ ServerRealmlistPacket
        (map realmlistEntry names)

-- mock data for now, TODO: fetch info from db
realmlistEntry :: ByteString -> ServerRealmlistEntry
realmlistEntry realmName =
    ServerRealmlistEntry
        0x01    -- icon
        0       -- lock (unlocked)
        0x04    -- flags
        (PNullString realmName) -- name
        (PNullString "185.56.175.154:8085") -- address
        0       -- population
        0x03    -- character count
        0x01    -- timezone
