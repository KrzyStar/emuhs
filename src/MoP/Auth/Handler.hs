{-# LANGUAGE OverloadedStrings #-}

module MoP.Auth.Handler where

import Control.Monad (void)
import Data.ByteString (ByteString)
import qualified Data.ByteString as BS
import Network.Socket (Socket)
import Network.Socket.ByteString (send)

import MoP.Common.Cryptography
import MoP.Common.Hash
import MoP.Common.Packet
import MoP.Auth.Database
import MoP.Auth.Opcode
import MoP.Auth.Packet
import MoP.Auth.Protocol

data AuthState
        = AuthInitial
        | AuthChallenged Username ChallengeSRP
        | AuthSuccess    Username ProofSRP
        | AuthFailed     Opcode   ResultCode
        | AuthDropped

instance Show AuthState where
    show AuthInitial          = "AuthInitial"
    show (AuthChallenged _ _) = "AuthChallenged"
    show (AuthSuccess    _ _) = "AuthSuccess"
    show (AuthFailed     _ _) = "AuthFailed"
    show AuthDropped          = "AuthDropped"

initChallenge :: ByteString -> IO AuthState
initChallenge username = do
    mPasshash <- fetchUser username
    case mPasshash of
        Just passhash -> AuthChallenged username <$> challengeSRP username passhash
        Nothing       -> return $ AuthFailed cmdLogonChallenge errAuthFailUnknownAccount

sendFail :: Socket -> AuthState -> IO AuthState
sendFail conn (AuthFailed cmd err) = send conn (BS.pack [cmd, 0, err]) >> return (AuthFailed cmd err)
sendFail _ state                   = return state

handleLogonChallenge :: Socket -> ClientLogonChallengePacket -> Username -> IO AuthState
handleLogonChallenge conn packet username =
    case fromEnum (clcBuild packet) of
        18414 -> do -- 5.4.8 MoP patch
            state <- initChallenge username
            case state of
                AuthChallenged _ csrp -> send conn (buildLogonChallenge csrp) >> return state
                AuthFailed     _ _    -> sendFail conn state
                _                     -> return AuthDropped
        _     -> sendFail conn (AuthFailed cmdLogonChallenge errAuthFailInvalidVersion)

handleLogonProof :: Socket -> ClientLogonProofPacket -> AuthState -> IO AuthState
handleLogonProof conn packet (AuthChallenged username csrp) =
    case proofSRP csrp (unPString $ clpHash_A packet) (unPString $ clpHash_Mc packet) of
        Just psrp -> do
            void $ send conn (buildLogonProof psrp)
            setSessionKey username (toBytes 40 $ _K psrp)
            return (AuthSuccess username psrp)
        Nothing   -> sendFail conn $ AuthFailed cmdLogonProof errAuthFailUnknownAccount
handleLogonProof _ _ _ = return AuthDropped


-- | TODO: implementation
handleReconnectChallenge :: Socket -> ClientReconnectChallengePacket -> Username -> IO AuthState
handleReconnectChallenge conn packet username = sendFail conn $ AuthFailed cmdReconnectChallenge errAuthFailServerBusy

-- | TODO: implementation
handleReconnectProof :: Socket -> ClientReconnectProofPacket -> AuthState -> IO AuthState
handleReconnectProof conn packet state = sendFail conn $ AuthFailed cmdReconnectProof errAuthFailServerBusy

handleRealmlist :: Socket -> AuthState -> IO AuthState
handleRealmlist conn state = do
    void $ send conn (buildRealmlist ["Realm #1", "Realm #2", "Realm #3"])
    return state

handlePacket :: Socket -> AuthState -> ByteString -> IO AuthState
handlePacket conn state bs =
    case (state, parsePacket bs) of
        -- client not yet challenged
        (_,                  Just (CLogonChallenge packet, username))     -> handleLogonChallenge conn packet username
        (_,                  Just (CReconnectChallenge packet, username)) -> handleReconnectChallenge conn packet username
        -- client not yet proven
        (AuthChallenged _ _, Just (CLogonProof packet, _))                -> handleLogonProof conn packet state
        (AuthChallenged _ _, Just (CReconnectProof packet, _))            -> handleReconnectProof conn packet state
        -- client authed
        (AuthSuccess _ _,    Just (CRealmlist _, _))                      -> handleRealmlist conn state
        -- corrupted packet
        (_,                  Nothing)                                     -> return AuthDropped
        -- invalid state for packet (should be dropped too?)
        _                                                                 -> return state
