{-# LANGUAGE OverloadedStrings #-}

module MoP.World.Handler where

import Control.Monad
import Data.ByteString (ByteString)
import qualified Data.ByteString as BS
import Data.Word
import Network.Socket (Socket)
import Network.Socket.ByteString (sendAll)

import MoP.Common.Hash
import MoP.Common.Packet

import MoP.World.Cryptography
import MoP.World.Database
import MoP.World.Opcode
import MoP.World.Packet
import MoP.World.Protocol

data SessionState = SessionInitial Word32 -- server seed
                  | SessionAuthed  ClientCrypto
                  | SessionFailed  ResultCode
                  | SessionDropped
                  deriving (Show)


sendStartSession :: Socket -> [ClientAddonDataEntry] -> SessionState -> IO SessionState
sendStartSession conn addons (SessionAuthed crypto) = do
    let (bs1, mcc1) = buildDanceStudio (Just crypto)
    let (bs2, mcc2) = buildAuthResponse "Realm #1" 1 mcc1
    let (bs3, mcc3) = buildGlueScreenStatus mcc2
    let (bs4, mcc4) = buildAddonInfo addons mcc3
    let (bs5, mcc5) = buildClientCacheVersion mcc4
    let (bs6, mcc6) = buildTutorialsData mcc5
    let (bs7, mcc7) = buildDisplayPromotion mcc6
    mapM_ (sendAll conn) [ bs1, bs2, bs3, bs4, bs5, bs6, bs7 ]
    SessionAuthed <$> maybe (fail "") return mcc7

handleAuthSession :: Socket -> ClientAuthSessionPacket -> SessionState -> IO SessionState
handleAuthSession conn packet (SessionInitial serverSeedNum) = do
    let username = casUsername packet
    mSessionKey <- getSessionKey username
    case mSessionKey of
        Nothing         -> return SessionDropped
        Just sessionKey -> do
            let clientCrypto = initCrypto sessionKey
            let serverSeed = toBytes 4 . Hash . fromIntegral . fromEnum $ serverSeedNum
            let clientSeed = toBytes 4 . Hash . fromIntegral . fromEnum $ casSeed packet

            let clientDigest = unPString $ casDigest packet
            let serverDigest = toBytes 20 $ sha1 $ BS.concat [username, BS.replicate 4 0x00, clientSeed, serverSeed, sessionKey]

            putStrLn $ "Client digest: " ++ show clientDigest
            putStrLn $ "Server digest: " ++ show serverDigest

            if clientDigest == serverDigest
                then do
                    sendStartSession conn (casAddonData packet) $ SessionAuthed clientCrypto
                    return $ SessionAuthed clientCrypto
                else do
                    void $ sendAll conn $ fst $ buildSessionFailed rAuthFailed $ Just clientCrypto
                    return $ SessionFailed rAuthFailed
handleAuthSession _ _ _ = return SessionDropped

handlePacket :: Socket -> SessionState -> ClientPacket -> IO SessionState
handlePacket conn state cPacket =
    case (state, cPacket) of
        -- client not yet authed
        (SessionInitial _, CAuthSession packet) -> handleAuthSession conn packet state
        _                                       -> return state
