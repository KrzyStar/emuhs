{-# OPTIONS_GHC -fno-warn-missing-signatures #-}
{-# LANGUAGE ScopedTypeVariables #-}

module MoP.World.Opcode where

import Data.Word

type Opcode = Word16
type ResultCode = Word8

cAuthSession        :: Opcode = 0x00B2

sAuthSession        :: Opcode = 0x0949
sAuthResponse       :: Opcode = 0x0ABA
sDanceStudio        :: Opcode = 0x1E9B
sGlueScreenStatus   :: Opcode = 0x12E1
sAddonInfo          :: Opcode = 0x160A
sClientCacheVersion :: Opcode = 0x002A
sTutorialsData      :: Opcode = 0x1B90
sDisplayPromotion   :: Opcode = 0x00A3

rAuthSuccess        :: ResultCode = 0x0C
rAuthFailed         :: ResultCode = 0x0D
rAuthUnknownAccount :: ResultCode = 0x15
