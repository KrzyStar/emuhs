{-# LANGUAGE OverloadedStrings #-}

module MoP.World.Server where

import Control.Monad
import Control.Monad.IO.Class
import Control.Monad.Random
import Control.Monad.State (StateT, execStateT, get, put)
import Data.ByteString (ByteString)
import qualified Data.ByteString as BS
import Data.Conduit
import Data.Conduit.Cereal
import Data.Serialize (Get)
import qualified Data.Serialize as DS
import Data.Streaming.Network (safeRecv)
import Data.Word
import Network.Socket (Socket, sClose)
import Network.Socket.ByteString (sendAll)
import Text.Printf
import Text.Show.Pretty

import MoP.Common.Hash
import MoP.World.Cryptography
import MoP.World.Handler
import MoP.World.Protocol

cryptoSocket :: Socket -> Source (StateT SessionState IO) ByteString
cryptoSocket socket = do
    state <- get
    bs <- liftIO $ safeRecv socket 4096
    if BS.null bs
        then liftIO $ sClose socket
        else case state of
            SessionAuthed crypto -> put (SessionAuthed newCrypto) >> yield result >> cryptoSocket socket
                where (newCrypto, result) = decryptRecv crypto bs
            _                    -> yield bs >> cryptoSocket socket

requestHandlerSink :: Socket -> Sink ClientPacket (StateT SessionState IO) ()
requestHandlerSink conn = do
    state <- get
    mPacket <- await
    liftIO . putStrLn $ printf "Current state: %s\nReceived packet: %s\n\n"
                        (ppShow state) (ppShow mPacket)
    case mPacket of
        Nothing             -> liftIO $ sClose conn
        Just (CUnknown _)   -> requestHandlerSink conn
        Just packet         -> do
            newState <- liftIO $ handlePacket conn state packet
            case newState of
                SessionDropped -> liftIO $ sClose conn
                _              -> put newState >> requestHandlerSink conn

getSessionSeed :: IO Word32
getSessionSeed = fromIntegral . unHash . flip getRandomHash 4 <$> newStdGen

handleConnection :: Socket -> IO ()
handleConnection conn = do
    putStrLn "Client connected"
    void $ sendAll conn "0\NULWORLD OF WARCRAFT CONNECTION - SERVER TO CLIENT\NUL"
    response <- safeRecv conn 50
    case response of
        "0\NULWORLD OF WARCRAFT CONNECTION - CLIENT TO SERVER\NUL" -> do
            seed <- getSessionSeed
            void $ sendAll conn $ fst (buildAuthSession seed Nothing)
            void $ flip execStateT (SessionInitial seed) $
                cryptoSocket conn $$ conduitGet (DS.get :: Get ClientPacket) =$= requestHandlerSink conn
        _ -> sClose conn
    putStrLn "Client disconnected"
