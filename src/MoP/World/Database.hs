{-# LANGUAGE OverloadedStrings #-}

module MoP.World.Database where

import Data.ByteString (ByteString)
import Database.PostgreSQL.Simple

import MoP.Common.Database

getSessionKey :: ByteString -> IO (Maybe ByteString)
getSessionKey username = do
    conn <- dbConnect
    res  <- query conn "select session_key from users where username = ?" (Only username) :: IO [Only (Maybe ByteString)]
    if null res then return Nothing
                else return $ fromOnly $ head res
