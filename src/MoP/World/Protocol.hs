{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

module MoP.World.Protocol where

import GHC.Generics

import Data.Bits
import Data.ByteString (ByteString)
import qualified Data.ByteString as BS
import Data.Map.Strict (Map)
import qualified Data.Map.Strict as Map
import Data.Serialize (Serialize, Get)
import qualified Data.Serialize as DS
import Data.Word

import MoP.World.Cryptography
import MoP.World.Packet
import MoP.World.Opcode

type PacketBuilder = Maybe ClientCrypto -> (ByteString, Maybe ClientCrypto)

data ClientPacket = CAuthSession ClientAuthSessionPacket
                  | CUnknown     UnknownPacket
                  deriving (Show, Generic)

parsers :: Map Opcode (Get ClientPacket)
parsers = Map.fromList
    [ (cAuthSession, CAuthSession <$> DS.get)
    ]

instance Serialize ClientPacket where
    get = do
        len <- fromEnum <$> DS.getWord16le
        cmd <- DS.lookAhead DS.getWord16le
        case Map.lookup cmd parsers of
            Just parser -> DS.skip 2 >> DS.isolate (len - 2) parser
            Nothing     -> DS.isolate len (CUnknown <$> DS.get)

parsePacket :: ByteString -> Maybe (ClientPacket, ByteString)
parsePacket bs = either (const Nothing) Just $ DS.runGetState (DS.get :: Get ClientPacket) bs 0

buildPacket :: Serialize a => Opcode -> a -> PacketBuilder

buildPacket opcode packet Nothing = (BS.append header bs, Nothing)
    where bs        = DS.runPut $ DS.put packet
          len       = fromIntegral $ BS.length bs + 2 :: Word16
          header    = DS.runPut $ DS.putWord16le len >> DS.putWord16le opcode

buildPacket opcode packet (Just crypto) = (BS.append header bs, Just newCrypto)
    where bs        = DS.runPut $ DS.put packet
          hLength   = fromIntegral $ BS.length bs      :: Word32
          hOpcode   = fromIntegral $ opcode .&. 0x1FFF :: Word32
          rawHeader = DS.runPut $ DS.putWord32le $ shiftL hLength 13 .|. hOpcode
          (newCrypto, header) = encryptSend crypto rawHeader

buildAuthSession :: Word32 -> PacketBuilder
buildAuthSession seed = buildPacket sAuthSession $ ServerAuthSessionPacket seed

buildSessionFailed :: ResultCode -> PacketBuilder
buildSessionFailed result = buildPacket sAuthResponse $ ServerSessionFailedPacket result

buildDanceStudio :: PacketBuilder
buildDanceStudio = buildPacket sDanceStudio ServerDanceStudioPacket

buildAuthResponse :: ByteString -> Word8 -> PacketBuilder
buildAuthResponse realmName realmId = buildPacket sAuthResponse $ ServerAuthResponsePacket realmName realmId
    [(1,0), (2,0), (3,0), (4,0), (5,0), (6,2), (7,0), (8,0), (9,0), (10,4), (11,0)] -- classes
    [(1,0), (2,0), (3,0), (4,0), (5,0), (6,0), (7,0), (8,0), (9,3), (10,1), (11,1), (22,3), (24,4), (25,4), (26,4)] -- races

buildGlueScreenStatus :: PacketBuilder
buildGlueScreenStatus = buildPacket sGlueScreenStatus ServerGlueScreenStatusPacket

buildAddonInfo :: [ClientAddonDataEntry] -> PacketBuilder
buildAddonInfo addons = buildPacket sAddonInfo $ ServerAddonInfoPacket addons

buildClientCacheVersion :: PacketBuilder
buildClientCacheVersion = buildPacket sClientCacheVersion $ ServerClientCacheVersionPacket 0

buildTutorialsData :: PacketBuilder
buildTutorialsData = buildPacket sTutorialsData ServerTutorialsDataPacket

buildDisplayPromotion :: PacketBuilder
buildDisplayPromotion = buildPacket sDisplayPromotion ServerDisplayPromotionPacket
