{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}

module MoP.World.Packet where

import GHC.Generics

import Codec.Compression.Zlib
import Control.Monad
import Data.Binary.Strict.BitGet (BitGet, runBitGet)
import qualified Data.Binary.Strict.BitGet as BG
import Data.Binary.BitPut (BitPut, runBitPut)
import qualified Data.Binary.BitPut as BP
import Data.ByteString (ByteString)
import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as BSL
import Data.Serialize
import Data.Word

import MoP.World.Cryptography
import MoP.World.Opcode
import MoP.Common.Packet

getFromBits :: Int -> BitGet a -> Get a
getFromBits n getter = do
    bitBuffer <- getByteString n
    either fail return $ runBitGet bitBuffer getter

putFromBits :: BitPut -> Put
putFromBits putter = putLazyByteString $ runBitPut putter

-- Unknown packet

data UnknownPacket = UnknownPacket
        { unkOpcode :: Word16
        , unkData   :: ByteString
        }
    deriving (Show, Generic)

instance Serialize UnknownPacket where
    get = do
        opcode <- getWord16le
        remBytes <- remaining
        UnknownPacket opcode <$> getByteString remBytes

-- Auth session challenge

data ServerAuthSessionPacket = ServerAuthSessionPacket
        { sasSeed :: Word32
        }
    deriving (Show, Generic)

instance Serialize ServerAuthSessionPacket where
    put pkt = do
        putWord16le 0
        replicateM_ 8 (putWord32le 0)
        putWord8 0x01
        putWord32le (sasSeed pkt)

data ClientAddonDataEntry = ClientAddonDataEntry
        { cadeName        :: PNullString
        , cadeUsingPubkey :: Word8
        , cadeCRC         :: Word32
        , cadeUrlFile     :: Word32
        }
    deriving (Show, Generic)

instance Serialize ClientAddonDataEntry where
    get = ClientAddonDataEntry
        <$> get
        <*> getWord8
        <*> getWord32le
        <*> getWord32le

data ClientAuthSessionPacket = ClientAuthSessionPacket
        { casDigest    :: PString 20
        , casSeed      :: Word32
        , casBuild     :: Word16
        , casAddonData :: [ClientAddonDataEntry]
        , casUsername  :: ByteString
        }
    deriving (Show, Generic)

instance Serialize ClientAuthSessionPacket where
    get = do
        skip 10
        [ d18, d14, d3, d4, d0 ]       <- replicateM 5 getWord8
        skip 4
        d11                            <- getWord8
        seed <- getWord32le
        d19                            <- getWord8
        skip 2
        [ d2, d9, d12 ]                <- replicateM 3 getWord8
        skip 12
        [ d16, d5, d6, d8 ]            <- replicateM 4 getWord8
        build <- getWord16le
        [ d17, d7, d13, d15, d1, d10 ] <- replicateM 6 getWord8

        addonRawDataLength <- getWord32le
        addonRawData <- BSL.drop 4 . BSL.fromStrict <$> getByteString (fromEnum addonRawDataLength)
        addonData <- either fail return $ flip runGetLazy (decompress addonRawData) $ do
            addonCount <- getWord32le
            replicateM (fromEnum addonCount) get

        usernameLength <- getFromBits 2 $ BG.skip 1 *> BG.getAsWord16 11
        username <- getByteString (fromEnum usernameLength)

        let digest = [ d0, d1, d2, d3, d4, d5, d6, d7, d8, d9, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19 ]

        return $ ClientAuthSessionPacket (PString $ BS.pack digest) seed build addonData username

data ServerSessionFailedPacket = ServerSessionFailedPacket
        { ssfResult :: ResultCode
        }
    deriving (Show, Generic)

instance Serialize ServerSessionFailedPacket where
    put pkt = do
        putWord16le 0
        putWord8 0
        putWord8 (ssfResult pkt)

data ServerDanceStudioPacket = ServerDanceStudioPacket
        {
        }
    deriving (Show, Generic)

instance Serialize ServerDanceStudioPacket where
    put = const $ do
        putWord32le 0x473216
        putWord32le 0x0C24E5
        putWord32le 0xA58757
        putWord32le 0x108D59
        putWord32le 0x80

data ServerAuthResponsePacket = ServerAuthResponsePacket
        { sarRealmName :: ByteString
        , sarRealmId   :: Word8
        , sarClasses   :: [(Word8, Word8)] -- (id, expansion)
        , sarRaces     :: [(Word8, Word8)] -- (id, expansion)
        }
    deriving (Show, Generic)

instance Serialize ServerAuthResponsePacket where
    put pkt = do
        putFromBits $ do
            BP.putBit True
            BP.putNBits 21 realmId
            BP.putNBits 8 realmNameLength
            BP.putNBits 8 realmNameLength
            BP.putBit True
            BP.putNBits 23 $ length (sarClasses pkt)
            BP.putNBits 21 (0x00 :: Word16) -- template count
            BP.putNBits 4  (0x00 :: Word8)
            BP.putNBits 23 $ length (sarRaces pkt)
            -- < serialize templates 1
            BP.putBit False
            BP.putBit False -- queued?
        putWord32le (fromIntegral realmId)
        putByteString realmName
        putByteString realmName
        forM_ (sarRaces pkt)   $ \r -> putWord8 (snd r) >> putWord8 (fst r)
        -- < serialize templates 2
        forM_ (sarClasses pkt) $ \r -> putWord8 (snd r) >> putWord8 (fst r)
        putWord32le 0
        putWord8 0x04    -- expansion (?)
        putWord32le 0x04 -- expansion (?)
        putWord32le 0
        putWord8 0x04    -- expansion (?)
        replicateM_ 3 $ putWord32le 0
        putWord8 rAuthSuccess
      where
        realmId         = sarRealmId pkt
        realmName       = sarRealmName pkt
        realmNameLength = BS.length realmName

data ServerGlueScreenStatusPacket = ServerGlueScreenStatusPacket
        {
        }
    deriving (Show, Generic)

instance Serialize ServerGlueScreenStatusPacket where
    put = const $ putWord8 0x00

data ServerAddonInfoPacket = ServerAddonInfoPacket
        { saiAddonData :: [ClientAddonDataEntry]
        }
    deriving (Show, Generic)

instance Serialize ServerAddonInfoPacket where
    put pkt = do
        putFromBits $ do
            BP.putNBits 18 (0x00 :: Word16) -- banned addon count
            BP.putNBits 23 (length $ saiAddonData pkt)
            forM_ (saiAddonData pkt) $ \a -> do
                BP.putBit False
                BP.putBit (cadeUsingPubkey a == 1)
                BP.putBit True
        forM_ (saiAddonData pkt) $ \_ -> do
            putByteString addonPubkey
            putWord8 0x01
            putWord32le 0x00
            putWord8 0x02

data ServerClientCacheVersionPacket = ServerClientCacheVersionPacket
        { sccvVersion :: Word32
        }
    deriving (Show, Generic)

instance Serialize ServerClientCacheVersionPacket where
    put pkt = putWord32le (sccvVersion pkt)

data ServerTutorialsDataPacket = ServerTutorialsDataPacket
        {
        }
    deriving (Show, Generic)

instance Serialize ServerTutorialsDataPacket where
    put = const $ replicateM_ 8 (putWord32le 0x00)

data ServerDisplayPromotionPacket = ServerDisplayPromotionPacket
        {
        }
    deriving (Show, Generic)

instance Serialize ServerDisplayPromotionPacket where
    put = const $ putWord32le 0x00
