module MoP.Common.Cryptography where

import Prelude

import Control.Monad.Random
import Data.ByteString (ByteString)
import qualified Data.ByteString as BS

import MoP.Common.Hash

data ChallengeSRP = ChallengeSRP
    { _s   :: Hash
    , _I   :: Hash
    , _v   :: Hash
    , _b   :: Hash
    , _B   :: Hash
    , _CRC :: Hash
    } deriving (Show)

hg, hk, hN :: Hash
hg = Hash 7
hk = Hash 3
hN = Hash 62100066509156017342069496140902949863249758336000796928566441170293728648119

challengeSRP :: ByteString -> ByteString -> IO ChallengeSRP
challengeSRP username passhash = do
    gen0 <- newStdGen
    gen1 <- newStdGen
    gen2 <- newStdGen

    let hs    = getRandomHash gen0 32 #%# hN
    let hb    = getRandomHash gen1 19 #%# hN
    let hCRC  = getRandomHash gen2 16 #%# hN

    let hp    = fromBytes passhash
    let hI    = sha1 username
    let hx    = sha1 $ BS.append (toBytes 32 hs) (toBytes 20 hp)
    let hv    = expMod hg hx hN
    let hbExp = expMod hg hb hN
    let hB    = (hv #*# hk #+# hbExp) #%# hN

    return $ ChallengeSRP hs hI hv hb hB hCRC

data ProofSRP = ProofSRP
    { _K  :: Hash
    , _M2 :: Hash
    } deriving (Show)

proofSRP :: ChallengeSRP -> ByteString -> ByteString -> Maybe ProofSRP
proofSRP csrp bsA bsMc = do
    let hs  = _s csrp
    let hI  = _I csrp
    let hv  = _v csrp
    let hb  = _b csrp
    let hB  = _B csrp
    let hA  = fromBytes bsA
    let hMc = fromBytes bsMc

    let hu = sha1 $ BS.append (toBytes 32 hA) (toBytes 32 hB)
    let hS = expMod (hA #*# expMod hv hu hN) hb hN
    let (bsS1, bsS2) = foldr (\x (ys, zs) -> (x:zs, ys)) ([], []) (BS.unpack $ toBytes 32 hS)
    let (ht1, ht2) = (sha1 $ BS.pack bsS1, sha1 $ BS.pack bsS2)
    let hK = fromBytes . BS.pack . concat $ BS.zipWith (\a b -> [a,b]) (toBytes 20 ht1) (toBytes 20 ht2)
    let hgN = sha1 (toBytes 1 hg) #^# sha1 (toBytes 32 hN)
    let hMs = sha1 $ BS.concat [toBytes 20 hgN, toBytes 20 hI, toBytes 32 hs, toBytes 32 hA, toBytes 32 hB, toBytes 40 hK]
    let hM2 = sha1 $ BS.concat [toBytes 32 hA, toBytes 20 hMs, toBytes 40 hK]

    if hMc == hMs then
        Just $ ProofSRP hK hM2
    else
        Nothing
