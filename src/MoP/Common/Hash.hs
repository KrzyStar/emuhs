module MoP.Common.Hash where

import Prelude hiding (null, init, last, reverse, length, replicate, tail, head)

import Control.Monad
import Control.Monad.Random
import qualified Crypto.Hash.SHA1 as SHA1
import Data.Bits
import Data.ByteString
import Data.Function
import Data.Word

newtype Hash = Hash { unHash :: Integer } deriving (Show, Eq)

-- | Binary operator converter for Hash type
hbin :: (Integer -> Integer -> Integer) -> Hash -> Hash -> Hash
hbin f = curry $ Hash . uncurry (f `on` unHash)

(#+#) :: Hash -> Hash -> Hash
(#+#) = hbin (+)
infixl 6 #+#

(#*#) :: Hash -> Hash -> Hash
(#*#) = hbin (*)
infixl 7 #*#

(#%#) :: Hash -> Hash -> Hash
(#%#) = hbin rem
infixl 7 #%#

(#^#) :: Hash -> Hash -> Hash
(#^#) = hbin xor
infixl 6 #^#

expMod' :: Integer -> Integer -> Integer -> Integer -> Integer
expMod' _ 0 _ r = r
expMod' b e m r | e `rem` 2 == 1 = expMod' (b * b `rem` m) (e `div` 2) m (r * b `rem` m)
expMod' b e m r = expMod' (b * b `rem` m) (e `div` 2) m r

expMod :: Hash -> Hash -> Hash -> Hash
expMod b e m = Hash $ expMod' (unHash b) (unHash e) (unHash m) 1

fromBytes :: ByteString -> Hash
fromBytes = Hash . byteValue
    where byteValue bs = if null bs then 0
          else byteValue (tail bs) * 256 + fromIntegral (head bs)

toBytes :: Int -> Hash -> ByteString
toBytes n = alignBytes n . unfoldr (\r -> if r == 0 then Nothing else Just (fromIntegral (r `rem` 256) :: Word8, r `quot` 256)) . unHash
    where alignBytes len bs = append (replicate (len - length bs) (0 :: Word8)) bs

sha1 :: ByteString -> Hash
sha1 = fromBytes . SHA1.hash

getRandomHash :: RandomGen g => g -> Int -> Hash
getRandomHash gen len = fromBytes $ getRandomBS gen len

getRandomBS :: RandomGen g => g -> Int -> ByteString
getRandomBS gen len = pack $ evalRand wordString gen
    where wordString = replicateM len getRandom :: RandomGen g => Rand g [Word8]
