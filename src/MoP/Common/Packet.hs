{-# LANGUAGE DataKinds #-}
{-# LANGUAGE KindSignatures #-}
{-# LANGUAGE ScopedTypeVariables #-}

module MoP.Common.Packet where

import Data.Proxy (Proxy(..))
import GHC.TypeLits (Nat, KnownNat, natVal)

import Control.Monad
import Data.ByteString (ByteString)
import qualified Data.ByteString as BS
import Data.Serialize

data PString            (len :: Nat) = PString            { unPString            :: ByteString } deriving Show
data PReverseString     (len :: Nat) = PReverseString     { unPReverseString     :: ByteString } deriving Show
data PNullString                     = PNullString        { unPNullString        :: ByteString } deriving Show
data PNullReverseString              = PNullReverseString { unPNullReverseString :: ByteString } deriving Show

instance KnownNat len => Serialize (PString len) where
    put (PString str) = putByteString str
    get = PString <$> getByteString len
        where len = fromIntegral $ natVal (Proxy :: Proxy len) :: Int

instance KnownNat len => Serialize (PReverseString len) where
    put (PReverseString str) = putByteString $ BS.reverse str
    get = PReverseString <$> (liftM BS.reverse . getByteString $ len)
        where len = fromIntegral $ natVal (Proxy :: Proxy len) :: Int

instance Serialize PNullString where
    put (PNullString str) = putByteString $ BS.snoc str 0
    get = PNullString <$> BS.reverse <$> getCRS
        where getCRS = BS.pack <$> getCRS' []
              getCRS' acc = getWord8 >>= (\b -> if b /= 0 then getCRS' (b:acc) else return acc)

instance Serialize PNullReverseString where
    put (PNullReverseString str) = putByteString $ BS.snoc (BS.reverse str) 0
    get = PNullReverseString <$> getCRS
        where getCRS = BS.pack <$> getCRS' []
              getCRS' acc = getWord8 >>= (\b -> if b /= 0 then getCRS' (b:acc) else return acc)
