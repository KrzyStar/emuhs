{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}

module MoP.Common.Database where

import Data.ByteString.Char8
import Database.PostgreSQL.Simple
import Text.Printf

dbHost, dbDatabase, dbUser, dbPassword :: String

dbHost     = "127.0.0.1"
dbDatabase = "emuhs"
dbUser     = "emuhs"
dbPassword = "emuhs"

dbConnect :: IO Connection
dbConnect = connectPostgreSQL $ pack $ printf "host='%s' dbname='%s' user='%s' password='%s'" dbHost dbDatabase dbUser dbPassword
