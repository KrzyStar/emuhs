{ stdenv, haskellngPackages, fetchFromGitHub }:

let
  haskellngPackages' = haskellngPackages.override {
    overrides = self: super: {
      binary-strict = super.binary-strict.override {
        mkDerivation = (attrs: self.mkDerivation ({
          pname = "binary-strict";
          version = "0.4.8.2";
          src = fetchFromGitHub {
            owner = "KrzyStar";
            repo = "binary-low-level";
            rev = "7fde3911a8419fd5ab4baff78f260ac77ef04639";
            sha256 = "02lx98ayvkxn0g5dd6vf6rj0zp772v6m9jlz2svr81n85qiwpksx";
          };
          homepage = "https://github.com/KrzyStar/binary-low-level";
          libraryHaskellDepends = [ super.mtl super.QuickCheck ];
          license = stdenv.lib.liceses.bsd3;
        }));
      };
    };
  };
  env = haskellngPackages'.ghcWithPackages (p: with p; [
    binary-strict
    cereal
    cereal-conduit
    cipher-rc4
    conduit
    conduit-extra
    containers
    cryptohash
    hex
    MonadRandom
    mtl
    network
    pretty-show
    postgresql-simple
    zlib
  ]);
in
  stdenv.mkDerivation {
    name = "emuHS";
    buildInputs = [env];
    shellHook = ''
      export NIX_GHC="${env}/bin/ghc"
      export NIX_GHCPKG="${env}/bin/ghc-pkg"
      export NIX_GHC_DOCDIR="${env}/share/doc/ghc/html"
      export NIX_GHC_LIBDIR=$( $NIX_GHC --print-libdir )
    '';
  }
